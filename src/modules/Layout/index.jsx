import Component from "./Layout.jsx";
import {connect} from "react-redux";
// import * as selectors from "../../selectors/selectors";
import * as selectors from "@selectors/selectors";
import * as actions from './actions';

const mapStateToProps = state => ({
  isPageToggle: selectors.getIsPageToggle(state),
});

const mapDispatchToProps = dispatch => ({
  togglePageSaga: (payload) => dispatch(actions.togglePageSaga(payload)),
  togglePage: payload => dispatch(actions.togglePage(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);

